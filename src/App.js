import React, {useState} from 'react';
import './App.css';
import Todo from './components/Todo.js'
import TodoForm from './components/TodoForm.js'
import TodoList from './components/TodoList.js'

function App() {

  const [todos, setTodos] = React.useState([
    {
      text: "Learn React",
      isCompleted: false
    },
    {
      text: "Meet friend for lunch",
      isCompleted: false
    },
    {
      text: "Learn React Native",
      isCompleted: false
    },
    {
      text: "PowerNap",
      isCompleted: false
    },
  ]);

  const [showFinished, setToggleFinished] = useState(false);

  const [completedTodos, setCompleted] = useState([...todos]);

 

  const addTodo = text => {
    const newTodos = [...todos, { text, isCompleted:false }];
    setTodos(newTodos);
    let toFinish = newTodos.filter(todo => todo.isCompleted === false);
    setCompleted([...toFinish])
    //setCompleted(newTodos)
  };

  const toggleComplete = index => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = !newTodos[index].isCompleted
   
    let toFinish = newTodos.filter(todo => todo.isCompleted === false);
    setCompleted([...toFinish])
    
  };

  const removeTodo = index => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
    setCompleted(newTodos)
  };

  const toggleShowFinished = () => {
    let toFinish = todos.filter(todo => todo.isCompleted === false);
    setCompleted([...toFinish])
    setToggleFinished(!showFinished);
  };

  console.log(`TO Finish `, completedTodos)
  console.log(`showFinished`, showFinished);

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, "0");
  var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = today.getFullYear();

  let date = dd + "-" + mm + "-" + yyyy;
  return (
    <div className="app">
      <div className="todo-list">
      <h1 className="mainTitle"> {date} </h1>
      <h2 className="mainTitle"> {completedTodos.length} of {todos.length} Tasks to finish! </h2>
      
      <TodoForm addTodo={addTodo} toggleShowFinished={toggleShowFinished} />
  
         {showFinished==true? 
         todos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={toggleComplete}
            removeTodo={removeTodo}
          />
        )):
         completedTodos.map((todo, index) => (
          <Todo
            key={index}
            index={index}
            todo={todo}
            completeTodo={toggleComplete}
            removeTodo={removeTodo}
          />))
  
        }
        
      </div>
    </div>
  );
}


export default App;