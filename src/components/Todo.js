import React, {useState} from 'react';
import { FaRegTrashAlt } from 'react-icons/fa';
import { FaCheckSquare } from 'react-icons/fa';

function Todo({ todo, index, completeTodo, removeTodo }) {
  const [isShown, setIsShown] = useState(false);
    return (
      <div
        className="todo-row"
        style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
        onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}
      >
        {index + 1}: {todo.text}
        <div >
          <FaCheckSquare onClick={() => completeTodo(index)} className="icons"/>
          {isShown && (
            <FaRegTrashAlt onClick={() => removeTodo(index)} className="icons"/>
          )}
        </div>
      </div>
    );
  }
export default Todo;