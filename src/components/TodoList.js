import React, {useState} from 'react'
import TodoForm from './TodoForm'
import Todo from './Todo'

function TodoList({ array }, { completeTodo }, { removeTodo }) {

    const displayList = (arr) => {
        arr.map((todo, index) => (
            <Todo
              key={index}
              index={index}
              todo={todo}
              completeTodo={completeTodo}
              removeTodo={removeTodo}
            />
          ))
        
    }
    
    return (
        <div>
            {displayList({ array })}
        </div>
    )
}

export default TodoList
