import React, {useState} from 'react';

function TodoForm({ addTodo, toggleShowFinished }) {
  const [value, setValue] = useState('');

  const handleSubmit = e => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="mainTodoForm">
        <input
          type="text"
          className="input"
          placeholder="What shall you do next?"
          value={value}
          onChange={e => setValue(e.target.value)}
        />
        <button className="todo-button" onClick={handleSubmit}>New Todo!</button>
        <label style={{display:"block", padding: ".5em 0"}} class="container">Show Finished Todos
  <input type="checkbox"  onClick={toggleShowFinished}></input>
  <span class="checkmark"></span>
</label>
      </div>
    </form>
  );
}

  export default TodoForm